<?php
if (!function_exists('is_admin')) {
    header('Status: 403 Forbidden');
    header('HTTP/1.1 403 Forbidden');
    exit();
}

if (!class_exists("Plugin_Audit_Settings")) :

class Plugin_Audit_Settings {

    public static $default_settings = array(
        'allowed_id' => 0,
        'content_frozen' => 0,
    );

    var $pagehook, $page_id, $settings_field, $options;

    function __construct() {
        $this->page_id = 'plugin_audit';
        // This is the get_options slug used in the database to store our plugin option values.
        $this->settings_field = 'plugin_audit_options';
        $this->options = get_option($this->settings_field);

        if (is_admin()) {
            add_action('admin_menu', array($this, 'admin_menu'), 20);
        }

        if ( function_exists('is_multisite') && is_multisite() && is_network_admin() ) {
            add_action('network_admin_menu', array($this, 'admin_menu')); 
        }
    }

    function admin_menu() {
        if ( ! is_multisite() && is_admin() ) {
            // Add a new submenu to the standard Settings panel
        $this->pagehook = $page = add_plugins_page(__('Plugin Auditor', 'plugin-auditor'), __('Plugin Auditor', 'plugin-auditor'), 'administrator', $this->page_id, array($this,'render'));

        // Include js, css, or header *only* for our settings page
        add_action("admin_print_scripts-$page", array($this, 'js_includes'));

        // add_action("admin_print_styles-$page", array($this, 'css_includes'));
        add_action("admin_head-$page", array($this, 'admin_head'));
        }

        if ( function_exists('is_multisite') && is_multisite() && is_network_admin() ) {
            // Add a new submenu to the standard Settings panel
        $this->pagehook = $page = add_plugins_page(__('Plugin Auditor', 'plugin-auditor'), __('Plugin Auditor', 'plugin-auditor'), 'administrator', $this->page_id, array($this,'render'));

        // Include js, css, or header *only* for our settings page
        add_action("admin_print_scripts-$page", array($this, 'js_includes'));

        // add_action("admin_print_styles-$page", array($this, 'css_includes'));
        add_action("admin_head-$page", array($this, 'admin_head'));
        }
        
    }

    function admin_head() {
?>
        <style>
            .settings_page_plugin_audit label { display:inline-block; width: 150px; }
        </style>
        <?php 

            // Plugin styles
            $handle = 'main';
            $src = plugins_url() . '/plugin-auditor/assets/css/main.css';
            $deps = array();
            $ver = '0.2';
            $media = 'all';

            // Enqueue CSS to sort tables
            wp_enqueue_style( $handle, $src, $deps, $ver, $media );


         ?>
<?php
    }

    function js_includes() {
        // Needed to allow metabox layout and close functionality.
        wp_enqueue_script('postbox');

        // Sort table attributes
        $handle = 'sortabble';
        $src = plugins_url() . '/plugin-auditor/assets/js/sorttable.js';
        $deps = array();
        $ver = '2.0.0';
        $in_footer = true;

        // Enqueue JavaScript to sort tables
        wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer );

        // Sort table attributes
        $handle = 'jquery.base64';
        $src = plugins_url() . '/plugin-auditor/assets/js/jquery.base64.js';
        $deps = array();
        $ver = '1.0.0';
        $in_footer = true;

        // Enqueue JavaScript to sort tables
        wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer );

        // Sort table attributes
        $handle = 'tableExport';
        $src = plugins_url() . '/plugin-auditor/assets/js/tableExport.js';
        $deps = array();
        $ver = '1.0.0';
        $in_footer = true;

        // Enqueue JavaScript to sort tables
        wp_enqueue_script( $handle, $src, $deps, $ver, $in_footer );
    }

    /*
        Sanitize our plugin settings array as needed.
    */
    function sanitize_theme_options($options) {
        $options['example_text'] = stripcslashes($options['example_text']);
        return $options;
    }

    /*
        Settings access functions.
    */
    protected function get_field_name($name) {

        return sprintf('%s[%s]', $this->settings_field, $name);
    }

    protected function get_field_id($id) {

        return sprintf('%s[%s]', $this->settings_field, $id);
    }

    protected function get_field_value($key) {

        return $this->options[$key];
    }

    /*
        Render settings page.
    */
    function render() {
        global $wp_meta_boxes, $wpdb;

        $table_name = $wpdb->prefix . 'plugin_audit';

        /* Query to select the entries that must be displayed in the table */
        $logs = $wpdb->get_results("SELECT * FROM $table_name WHERE status = 1 AND plugin_path <> 'plugin-auditor/plugin-audit.php' ORDER BY `timestamp` DESC");

        /* This query let us know if the date of the plugins are the same of the plugin auditor. If true, the user will be displayed as "Unkown" */
        $query_date = $wpdb->get_var( "SELECT `timestamp` FROM $table_name WHERE plugin_path = 'plugin-auditor/plugin-audit.php'" );

        if(isset($_POST['add_note'])) {
            if(!empty($_POST['note'])) {
                $wpdb->update(
                    $table_name,
                    array('note' => sanitize_text_field($_POST['note'])),
                    array('id' => intval($_POST['log_id'])),
                    array('%s'),
                    array('%d')
                );
            }
        } ?>
        <h1><?php _e( 'Plugin Auditor', 'plugin-auditor' ) ?></h1>
        <p><?php _e( 'Below is a record of all installed plugins, including details of who installed them, when they were installed and why. This information should make it easier to maintain your WordPress website.', 'plugin-auditor' ); ?></p>
        <p><?php _e( 'If you are adding a comment to a plugin, please ensure that you state the reason for adding the plugin, not simply describe the functionality of the plugin. You will thank yourself for this later!', 'plugin-auditor' ); ?></p>
        <button class="button button-secondary"><a class="no-style" href="#" onClick ="$('#main-table').tableExport({type:'excel',escape:'false'});">Export data to Excel</a></button>
        
        <div class="wrap">

            <table class="sortable wp-list-table widefat fixed posts" id="main-table">
                <thead>
                    <tr>
                        <th scope="col" class="manage-column"><?php _e( 'Plugin', 'plugin-auditor' ) ?></th>
                        <th scope="col" class="manage-column"><?php _e( 'User', 'plugin-auditor' ) ?></th>
                        <th scope="col" class="manage-column sorttable_numeric"><?php _e( 'Date Installed', 'plugin-auditor' ) ?></th>
                        <th scope="col" class="manage-column"><?php _e( 'Reason for Installation', 'plugin-auditor' ) ?></th>
                        <!-- <th scope="col" class="manage-column sorttable_nosort hide"><?php _e( 'Status', 'plugin-auditor' ) ?></th> -->
                        <th scope="col" class="manage-column sorttable_nosort"><?php _e( 'Manage Comments', 'plugin-auditor' ) ?></th>
                    </tr>
                </thead>
                <tbody id="the-list">
                    <?php
                    foreach($logs as $log) {
                        $plugin_data = json_decode($log->plugin_data);
                        $old_plugin_data = json_decode($log->old_plugin_data);
                    ?>
                    <tr id="log-<?php echo $log->id ?>" class="log-<?php echo $log->id ?> type-log">
                        <td>
                            <span title="<?php echo $plugin_data->Name; ?>"><?php echo $plugin_data->Name; ?>
                            (<?php echo $plugin_data->Version; ?>)
                            </span>
                        </td>
                        <td><?php
                            $user_info = get_userdata($log->user_id); 
                            if ( $query_date == $log->timestamp && $plugin_data->Name != 'Plugin Auditor' ) {
                                _e( 'Unkown User', 'plugin-auditor' );
                            } else if ( ! $user_info ) {
                                _e( 'Unkown User', 'plugin-auditor' );
                            } else {
                                if($user_info->first_name && $user_info->last_name):
                                    echo $user_info->first_name . ' ' . $user_info->last_name;
                                elseif ($user_info->first_name && !$user_info->last_name):
                                    echo $user_info->first_name;
                                else:
                                    echo $user_info->user_login;
                                endif;
                            } ?></td>
                        <td><?php echo $log->timestamp; ?></td>
                        <td><?php echo $log->note; ?></td>
                        <?php
                        // if ( $log->status == 1 ){
                        //     $status = __( 'Installed', 'plugin-auditor' );
                        // } else if ( $log->status == 2 ) {
                        //     $status = __( 'Activated', 'plugin-auditor' );
                        // } else if ( $log->status == 3 ) {
                        //     $status = __( 'Deactivated', 'plugin-auditor' );
                        // } else if ( $log->status == 4 ) {
                        //     $status = __( 'Version Change', 'plugin-auditor' );
                        // } else if ( $log->status == 5 ) {
                        //     $status = __( 'Uninstalled', 'plugin-auditor' );
                        // }
                        ?>
                        <td>
                            <form action="" method="post">
                                <input type="hidden" name="log_id" value="<?php echo $log->id ?>">
                                <input type="hidden" name="edit_note" value="true">
                                <button type="submit" class="button button-primary add-or-edit-comment" style="vertical-align: top;">
                                <?php $log->note == NULL ? _e( 'Add comment', 'plugin-auditor' ) : _e( 'Edit comment', 'plugin-auditor' )
                                ?>
                                </button>
                            </form>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
            <br>
            
        </div>
    <?php }

    function print_title($var) {
        $search = array('stdClass Object', '<', '>', '"');
        return trim(trim(str_replace($search, '', strip_tags(print_r($var, true)))), '()');
    }

} // end class
endif;
?>